#!/bin/bash
cd ..
source "$(pwd)/utils.sh"
cd -

logo_start "logo_legacy.sh"
check_jq
check_nvm
check_screen
kill_screens

# File containing the JSON list of repositories and their Node.js versions
NODE_FILE="node.json"

node_version=$(jq -r '.iap' node.json)

# start virtual terminal session
screen -S iap-mono -d -m
screen -S iap-mono -X stuff $"source ~/.bash_profile && nvm use $node_version && cd iap && node ./server.js"
echo ""
print_magenta "iap-mono"
print_green "[GNU] screen iap-mono session created"


screen -S iag-legacy -d -m
screen -S iag-legacy -X stuff $'cd automation-gateway && ./bootDevEnvironment.sh'
echo ""
print_blue "iag-legacy"
print_green "[GNU] screen iag-legacy session created"

# reattach screen
echo ""
screen -ls
echo ""