#!/bin/bash
cd ..
source "$(pwd)/utils.sh"
cd -

logo_start "logo_legacy.sh"
check_jq

# File containing the JSON list of repositories to clone
REPO_FILE="./repos.json"

# Directory to clone the repositories into
TARGET_DIR="./"

# Create the target directory if it doesn't exist and cd
mkdir -p $TARGET_DIR
cd $TARGET_DIR

print_cyan "=== Cloning Repos ==="

# Loop through each repository in the JSON file
jq -r 'to_entries[] | "\(.key) \(.value)"' "$REPO_FILE" | while read -r REPO_URL BRANCH; do
    # Extract the repo name from the URL (everything after the last '/')
    REPO_NAME=$(basename "$REPO_URL" .git)

    if [ -d "$REPO_NAME" ]; then
        # If the directory exists, enter it and update the repository
        cd "$REPO_NAME"
        print_green "Directory $REPO_NAME exists. Checking out branch $BRANCH and pulling latest changes."
        git checkout "$BRANCH"
        git pull origin "$BRANCH"
        cd ..
    else
        # If the directory does not exist, clone the repository and check out the specified branch
        if git clone -b "$BRANCH" "$REPO_URL"; then
            print_green "Successfully cloned $REPO_NAME and checked out branch $BRANCH"
        else
            print_red "Failed to clone $REPO_NAME"
        fi
    fi
done
