# Source nvm script
cd ..
source "$(pwd)/utils.sh"
cd -

logo_start "logo_legacy.sh"
check_nvm
check_jq

# File containing the JSON list of repositories and their Node.js versions
NODE_FILE="node.json"

# Directory containing the cloned repositories
TARGET_DIR="."

print_cyan "=== Installing Repos ==="

# Loop through each repository in the JSON file
jq -r 'to_entries[] | "\(.key) \(.value)"' "$NODE_FILE" | while read -r REPO NODE_VERSION; do
    REPO_DIR="$TARGET_DIR/$REPO"

    if [ -d "$REPO_DIR" ]; then
        echo "Switching to Node.js version $NODE_VERSION for $REPO"
        if [ -z "$NODE_VERSION" ]; then
            # executes when no node version defined ""
            print_yellow "No Node Version specified for $REPO"
        elif nvm use "$NODE_VERSION"; then
            echo "Running npm install in $REPO_DIR"
            cd "$REPO_DIR"
            if npm install; then
                print_green "Successfully ran npm install in $REPO"
            else
                print_red "Failed to run npm install in $REPO"
            fi
            cd - > /dev/null
        else
            print_red "Failed to switch to Node.js version $NODE_VERSION for $REPO"
        fi
    else
        print_yellow "Directory $REPO_DIR does not exist. Skipping."
    fi
done

# install IAP
cd ./iap
npm install

# build services
print_cyan "=== Building Services ==="
npm run build:services