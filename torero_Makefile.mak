# Copyright 2023 Itential Inc. All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential

export GOOS        := $(shell uname | tr '[:upper:]' '[:lower:]')
export GOARCH      := amd64
export CGO_ENABLED := 0

TORERO_CONTAINER_RUNTIME ?= docker
TORERO_PYTHON_INTERPRETER ?= python
TORERO_PYTHON_PIP ?= pip

.PHONY: build \
	clean \
	config \
	containers \
	coverage \
	docs \
	install \
	proto \
	run \
	test \
	gendocs \
	venv \
	licenses

help:
	@echo "Available targets:"
	@echo "  build      - Builds the torero application binary"
	@echo "  clean      - Cleans the development environment"
	@echo "  clean-docs - Cleans the docs virtual environment"
	@echo "  config     - Display the runtime config"
	@echo "  container  - Build torero container utilizing ./build/torero.conf"
	@echo "  coverage   - Run test coverage report"
	@echo "  docs       - Build and launch local documentation server"
	@echo "  gendocs    - Generate CLI documentation for torero"
	@echo "  install    - Install tooling dependencies"
	@echo "  proto      - Build protocol buffers"
	@echo "  run        - Build and run the torero container"
	@echo "  server     - Start a local server instance"
	@echo "  test       - Run all tests"
	@echo "  venv       - Build the docs virtual environment"
	@echo "  licenses   - Ensures that licenses exist on every code file"

config: 
	@echo "GOOS=${GOOS}"
	@echo "GOARCH=${GOARCH}"
	@echo "CGO_ENABLED=${CGO_ENABLED}"
	@echo "TORERO_CONTAINER_RUNTIME=$(TORERO_CONTAINER_RUNTIME)"
	@echo "TORERO_PYTHON_INTERPRETER=$(TORERO_PYTHON_INTERPRETER)"
	@echo "TORERO_PYTHON_PIP=$(TORERO_PYTHON_PIP)"
	@echo

clean:
	@if [ -d bin ]; then rm -rf bin; fi

clean-docs:
	@if [ -d venv ]; then rm -rf venv; fi

container: 
	@if [ ! -f ./build/torero.conf ]; then cp ./docs/example-torero.conf ./build/torero.conf; fi
	@${TORERO_CONTAINER_RUNTIME} build \
		--ssh default \
		-f build/Containerfile \
		-t torero:latest \
		${PWD}

coverage:
	@scripts/test.sh coverage

build:
	@go build \
		-v \
		-o bin/torero \
		-ldflags="-X 'gitlab.com/itential/automation-gateway/torero/internal/torero.Sha=$$(git rev-parse --short HEAD)' -X 'gitlab.com/itential/automation-gateway/torero/internal/torero.User=$$(id -u -n)' -X 'gitlab.com/itential/automation-gateway/torero/internal/torero.Time=$$(date)' -X 'gitlab.com/itential/automation-gateway/torero/internal/torero.Version=$$(git tag | sort -V | tail -1)'"

install:
	@go get \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
		google.golang.org/protobuf/cmd/protoc-gen-go \
		google.golang.org/grpc/cmd/protoc-gen-go-grpc
	@go install \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
		github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
		google.golang.org/protobuf/cmd/protoc-gen-go \
		google.golang.org/grpc/cmd/protoc-gen-go-grpc

proto: 
	@protoc \
		--go_out=${PWD} \
		--go_opt=paths=source_relative \
		--go-grpc_out=${PWD} \
		--go-grpc_opt=paths=source_relative \
		--grpc-gateway_out=${PWD} \
		--grpc-gateway_opt=logtostderr=true \
		--grpc-gateway_opt=paths=source_relative \
		api/core/v1/*.proto \
		api/runner/v1/*.proto

run: container
	@${TORERO_CONTAINER_RUNTIME} run \
		--rm \
		--name torero \
		-p 50051:50051 \
		-e TORERO_SERVER_LISTEN_ADDRESS=0.0.0.0 \
		-e TORERO_APPLICATION_WORKING_DIR=/opt/torero \
		-e TORERO_APPLICATION_AUTO_ACCEPT_EULA=true \
        -e TORERO_APPLICATION_MODE=local \
		torero:latest

server: 
	@go run main.go server

test:
	@scripts/test.sh unittest
	@scripts/test.sh judotest

# TODO improve this method
gendocs:
	@env TORERO_TERMINAL_NO_COLOR=true env DOCUMENTATION_GENERATION_MODE="true" go run ./main.go "t>gendocs"

venv:
	${TORERO_PYTHON_INTERPRETER} -m venv venv; \
	source venv/bin/activate; \
	${TORERO_PYTHON_PIP} install -r requirements/requirements.txt

# Builds the documentation and starts a local docs server.  For the docs
# environment to build it expects both `python` and `pip` to be available in
# the path.  These values can be changed by setting TORERO_PYTHON_INTERPRETER
# and TORERO_PYTHON_PIP respectively.
docs: venv
	source venv/bin/activate; \
	mkdocs serve -f mkdocs.yaml

licenses:
	@go-licenses report . --template ./tools/license-attributions/template.tpl --ignore gitlab.com/itential > license-attributions.md
	@go run ./tools/copyrighter/main.go