# colored_text.sh

# ANSI color codes
RED='\e[31m'
GREEN='\e[32m'
YELLOW='\e[33m'
BLUE='\e[34m'
MAGENTA='\e[35m'
CYAN='\e[36m'
WHITE='\e[37m'
RESET='\e[0m'

# Function to print colored text
print_red() {
    echo -e "${RED}$1${RESET}"
}

print_green() {
    echo -e "${GREEN}$1${RESET}"
}

print_yellow() {
    echo -e "${YELLOW}$1${RESET}"
}

print_blue() {
    echo -e "${BLUE}$1${RESET}"
}

print_magenta() {
    echo -e "${MAGENTA}$1${RESET}"
}

print_cyan() {
    echo -e "${CYAN}$1${RESET}"
}

print_white() {
    echo -e "${WHITE}$1${RESET}"
}
