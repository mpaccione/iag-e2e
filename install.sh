source ./utils.sh

logo_start "logo.sh"
kill_containers
check_nvm
check_jq

# File containing the JSON list of repositories and their Node.js versions
NODE_FILE="node.json"

# Directory containing the cloned repositories
TARGET_DIR="."

print_cyan "=== Installing Repos ==="
echo ""

# Loop through each repository in the JSON file
jq -r 'to_entries[] | "\(.key) \(.value)"' "$NODE_FILE" | while read -r REPO NODE_VERSION; do
    REPO_DIR="$TARGET_DIR/$REPO"

    if [ -d "$REPO_DIR" ]; then
        echo "Switching to Node.js version $NODE_VERSION for $REPO"
        if [ -z "$NODE_VERSION" ]; then
            # executes when no node version defined ""
            print_yellow "No Node Version specified for $REPO"
        elif nvm use "$NODE_VERSION"; then
            echo "Running npm install in $REPO_DIR"
            cd "$REPO_DIR"

            # only install for others
            if npm install; then
                echo ""
                print_green "Successfully ran npm install in $REPO"

                # build for iap
                if [ $REPO == "iap" ]; then
                    if npm run build:services; then
                        echo ""
                        print_green "Successfully ran npm install and build:services in $REPO"
                    else
                        print_red "Failed to run npm build in $REPO"
                    fi
                fi

            else
                print_red "Failed to run npm install in $REPO"
            fi
            cd - >/dev/null
        else
            print_red "Failed to switch to Node.js version $NODE_VERSION for $REPO"
        fi
    else
        print_yellow "Directory $REPO_DIR does not exist. Skipping."
    fi

    echo ""
done

echo ""

print_cyan "=== Copying Commander into IAP (requires sudo) ==="

if npm --prefix ./app-iag_commander run build && sudo cp -R ./app-iag_commander ./iap/services/; then
    echo ""
    print_green "Directory copied successfully."
else
    echo ""
    echo "Failed to copy app-iag_commander into IAP"
fi

# setup torero/gateway5
echo ""
print_cyan "=== Torero/Gateway5 Env Setup ==="
echo ""
# Configure Git to use SSH instead of HTTP for GitLab
export GOPRIVATE=gitlab.com/itential/automation-gateway/torero
export TORERO_APPLICATION_MODE=server
git config --global url."ssh://git@gitlab.com/".insteadOf "https://gitlab.com/"

# Check if the ~/.netrc file exists
if [ ! -f ~/.netrc ]; then
    echo "machine gitlab.com
login <insert username>
password <insert personal access token>" >~/.netrc
    chmod 0400 ~/.netrc
    print_yellow "~/.netrc file created please populate values"
else
    print_green "~/.netrc file already exists."
fi

# copy certs
if [ ! -f ./gateway5/gateway.crt ]; then
    echo "-----BEGIN CERTIFICATE-----
MIIDMzCCAhugAwIBAgIBATANBgkqhkiG9w0BAQsFADA5MQswCQYDVQQGEwJVUzEW
MBQGA1UEChMNYXV0b21hdGlvbk9yZzESMBAGA1UEAwwJY2x1c3Rlcl8xMB4XDTI0
MDYyMTAxMjgzMVoXDTI1MDYyMTAxMjgzMVowOTELMAkGA1UEBhMCVVMxFjAUBgNV
BAoTDWF1dG9tYXRpb25PcmcxEjAQBgNVBAMMCWNsdXN0ZXJfMTCCASIwDQYJKoZI
hvcNAQEBBQADggEPADCCAQoCggEBALaulenGkBzKzLBSVl12/JGPs4rfT7d9Dmzt
lbd22LTUs0cAFeK7AN6/wh6wK5xMi3nZIZxrtBRR77vZ4rwtNdwwhpvmItNRhdK1
l7fyEXr5BiTR0lFu5W76GzxEadGgbESdscQSftVNFcm4lmoZEz1b5CxQDuW1lvFE
pN8bjNyFSWzFPmJmu8R1c93vruo3fYgEcBX3c5wtP/DZjCoJQwNz3+cTVEuuntBK
TjOOFL/ecMDvEeza1KhTQXZJgByykruZBW8j8DQryN/np6VuN3AFcEs+0+ygQF/R
qfNCc13dyTYHJUryDildPaSCLUQAASrUCrzILu9z4PT8nPINLOECAwEAAaNGMEQw
DgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMAwGA1UdEwEB/wQC
MAAwDwYDVR0RBAgwBocEfwAAATANBgkqhkiG9w0BAQsFAAOCAQEAKpeLJMQ4vjkH
WftAJ+KiNV3Q31utIDm0mnGYPeK5CpjPcqAhvHgNNd4/ZXH81eKcFARTNKiRq7pu
tPEQADO3bplmi7g/wcWEo7l5rV83k//UzDDVvCvpM9bDAkqkRVKaaT7Vkgj8DjpQ
F6idlGAO7wfxjDhnfZ+ZuS962yYHLQyfpVzjERntVKpCTMLNA7GVNvb80RiIOYXc
AKzZdWzFV1OzOYgttA/SwB2hMGzlUNsicZSmdIOmC01lQjdegZQTDy3ZcDxE1SSx
MBqPgBN5T9gQ7CrLOD8INwP1DmAFsk+lhSD5ic4XEmGgrrnIaFxL5IYqXItkkQGr
Hr7YHu1GzA==
-----END CERTIFICATE-----" >>./gateway5/build/gateway.crt
    print_yellow "Creating certificate for Gateway 5"
else
    print_green "./gateway5/build/gateway.crt file already exists."
fi

# create configuration file
if [ ! -f ./gateway5/build/gateway.conf ]; then
    echo "[connection]
enabled = true
host = localhost
port = 5173
certificate_file = ./etc/gateway/certs/gateway.crt
cluster_id = 1" >>./gateway5/build/gateway.conf
    print_yellow "Creating ./gateway5/build/gateway.conf file"
else
    print_green "./gateway5/build/gateway.conf file already exits"
fi

echo ""
