#!/bin/bash
source "./colored_text.sh"

logo_start() {
    echo ""
    source "$1"
}

check_bash() {
    # Check if Bash version is 4.0 or later
    if [[ "${BASH_VERSINFO:-0}" -lt 4 ]]; then
        print_red "This script requires Bash version 4.0 or later."
        exit 1
    fi
}

check_jq() {
    if ! command -v jq &>/dev/null; then
        # Check if jq is installed
        print_red "jq is not installed. Please install jq to proceed."
        exit 1
    fi
}

check_nvm() {
    # Source nvm script
    if [ -f "$HOME/.nvm/nvm.sh" ]; then
        source "$HOME/.nvm/nvm.sh"
    elif [ -f "$HOME/.bash_profile" ]; then
        source "$HOME/.bash_profile"
    elif [ -f "$HOME/.bashrc" ]; then
        source "$HOME/.bashrc"
    elif [ -f "$HOME/.profile" ]; then
        source "$HOME/.profile"
    else
        print_red "NVM not found. Please install NVM or ensure it is sourced correctly."
        exit 1
    fi
}

check_screen() {
    # Check if screen is installed
    if ! command -v screen &>/dev/null; then
        print_red "Screen terminal multiplexer is not installed. Please install screen and try again. brew install screen"
        exit 1
    fi
}

kill_screens() {
    # screen cleanup
   sessions_to_kill=("gateway5" "app-ottoman" "gateway-management-ui" "torero")

    for session_name in "${sessions_to_kill[@]}"; do
        # List all screen sessions and find the one that matches the session_name
        screen -ls | grep "$session_name" | while read -r line; do
            # Extract the session ID
            session_id=$(echo $line | awk '{print $1}')
            
            # Kill the session
            screen -S $session_id -X quit
            print_yellow "[$session_id] Screen Killed"
        done
    done
}

kill_containers() {
    # docker cleanup
    if [ -n "torero" ]; then
        docker rm -rf "torero"
        print_yellow "Torero container deleted"
    elif [ -n "gateway5"]; then
        docker rm -rf "gateway5"
        print_yellow "Gateway5 container deleted"
    fi
}

contains_value() {
    local value="$1"
    shift
    local array=("$@")

    for element in "${array[@]}"; do
        if [[ "$element" == "$value" ]]; then
            return 0 # Return success
        fi
    done

    return 1 # Return failure
}

start_services() {
    # File containing the JSON list of repositories and their Node.js versions
    NODE_FILE="node.json"

    # Directory containing the cloned repositories
    TARGET_DIR="$(pwd)"

    REPO_DIR="$TARGET_DIR/$REPO"

    print_cyan "=== Starting Repos ==="

    # Loop through each repository in the JSON file
    jq_output=$(jq -r 'to_entries[] | "\(.key) \(.value)"' "$NODE_FILE")

    while read -r entry; do
        REPO=$(echo "$entry" | awk '{print $1}')
        NODE_VERSION=$(echo "$entry" | awk '{print $2}')
        skip_service=0

        for excluded_service in "$@"; do
            if contains_value "$REPO" "$excluded_service"; then
                skip_service=1
            fi
        done

        if [ "$skip_service" == 0 ]; then
            # color labeling
            echo ""
            case $REPO in
            app-iag_commander | gateway5)
                print_blue "$REPO"
                ;;
            torero)
                print_magenta "$REPO"
                ;;
            *)
                print_cyan "$REPO"
                ;;
            esac

            # node versioning and screen init
            if ! screen -ls | grep -q "$REPO"; then

                if [ -d "$REPO_DIR" ]; then
                    if [ -z "$NODE_VERSION" ]; then
                        # executes when no node version defined ""
                        init_screen "$REPO"
                    elif nvm use "$NODE_VERSION"; then
                        # executes when node version defined (node.json)
                        init_screen "$REPO"
                    else
                        print_yellow "Failed to switch to Node.js version $NODE_VERSION for $REPO"
                    fi
                else
                    print_red "Directory $REPO_DIR does not exist. Skipping."
                fi
            else
                print_green "Screen already exists"
            fi
        fi

    done <<<"$jq_output"

    echo ""
}
