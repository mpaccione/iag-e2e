# IAG-E2E
Supports IAG and Legacy IAG

# IAG
# VIDEO WALKTHROUGH
https://www.loom.com/share/48877325ed9c42659dc53ecc3e541a2a

## Cloning Repos - clone.sh
Repos are dynamically created in "git-uri":"branch" key value format.

ENV_NOTE: The ABS .env file is created during this process.

```
nano repos.json

{
  "git@gitlab.com:itential/automation-gateway/gateway5.git" : "devel",
  "git@gitlab.com:itential/automation-gateway/torero.git": "devel",
  "git@gitlab.com:itential/cloud/gateway-management-ui.git": "master",
  "git@gitlab.com:itential/labs/app-iag_commander.git": "master",
  "git@gitlab.com:itential/labs/app-ottoman.git": "master",
  "git@gitlab.com:itential/product-engineering/iap.git": "master"
}

```

## NPM Install with NVM - install.sh
Repos are dynamically installed in "project-name":"node-version" key value format.
You must have [NVM](https://github.com/nvm-sh/nvm) installed for this script to execute correctly.

ENV_NOTE: Kills non-iap screens
ENV_NOTE: Copies built app-iag_commander files into iap
ENV_NOTE: Configures Git to use SSH
ENV_NOTE: Creates ~./netrc file
ENV_NOTE: Creates gateway5 build/gateway.conf file

```
nano node.json

{
    "app-iag_commander": "20.3.0", 
    "app-ottoman": "20.3.0",
    "iap": "20.3.0", 
    "gateway-management-ui": "20.3.0",
    "gateway5": "",
    "torero": ""
}
```

## Execution with GNU Screen - start.sh
The start.sh script will create two gnu screen sessions with iap and iag.

ENV_NOTE: Kills non-iap screens
ENV_NOTE: Creates torero makefile with fixed default
ENV_NOTE: Creates torero.conf file with fixed default
ENV_NOTE: Creates gateway5 dockerfile override

```
There are screens on:
	40254.iap	(Detached)
	57600.gateway5	(Detached)
	57450.app-ottoman	(Detached)
	57585.gateway-management-ui	(Detached)
5 Sockets in /tmp/uscreens/S-mpaccione.
```

You can switch between sessions with
```
screen -r iap
screen -r gateway5
screen -r app-ottoman
screen -r gateway-management-ui
```

Upon switching you will must press ENTER to execute the prepopulated commands.






# LEGACY IAG
# VIDEO WALKTHROUGH
https://www.loom.com/share/367e21376ec947c596981d599cda080e

## Cloning Repos - clone.sh
Repos are dynamically created in "git-uri":"branch" key value format.

```
nano repos.json

{
  "git@gitlab.com:itential/automation-gateway/automation-gateway.git": "master",
  "git@gitlab.com:itential/product-engineering/iap.git": "master"
}

```

## NPM Install with NVM - install.sh
Repos are dynamically installed in "project-name":"node-version" key value format.
You must have [NVM](https://github.com/nvm-sh/nvm) installed for this script to execute correctly.

```
nano node.json

{
    "iap": "20.3.0"
}
```

## Execution with GNU Screen - start.sh
The start.sh script will create two gnu screen sessions with iap and iag.

```
mpaccione@Michael-Paccione-MacBook-Pro- legacy % ./start.sh
No matching processes belonging to you were found
There are screens on:
	69045.iag-legacy	(Detached)
	69040.iap-mono	(Detached)
2 Sockets in /tmp/uscreens/S-mpaccione.
```

You can switch between sessions with
```
screen -r iag
screen -r iap
```

Upon switching you will must press ENTER to execute the prepopulated commands.
