#!/bin/bash
source utils.sh

logo_start "logo.sh"
check_bash
check_jq
check_nvm
check_screen
kill_screens

# start virtual terminal session for clientside
init_screen() {
    local repo="$1"

    screen -S $repo -dm
    screen -S $repo -X stuff $"${COMMANDS[$REPO]}"

    print_green "[GNU] screen ${repo} session created"
}

# create starting command object
declare -A COMMANDS
COMMANDS[app-iag_commander]="cd app-iag_commander && npm run dev"
COMMANDS[app-ottoman]="cd app-ottoman && npm run dev"
COMMANDS[iap]="cd iap && node server.js"
COMMANDS[gateway-management-ui]="cd gateway-management-ui && npm run dev"
COMMANDS[gateway5]="cd gateway5 && docker compose up"
COMMANDS[torero]="cd torero && docker compose up"

# Interactive prompt for torrero or gateway5
read -n 1 -p "Enter [t]orero or [g]ateway5: " server_choice
echo ""

case $server_choice in
  t)
    print_magenta "=== torero ==="
    # torero dependency
    cp torero_Makefile.mak ./torero/Makefile.mak
    chmod a+rw ./torero/Makefile.mak

    cp ./torero_dev.conf ./torero/build/torero.conf
    chmod a+rw ./torero/build/torero.conf

    excluded_services=("app-iag_commander" "gateway5")
    start_services "${excluded_services[@]}"
    ;;
  g)
    print_blue "=== gateway5 ==="
    # gateway5 dependency
    cp gateway5-override.yaml ./gateway5/docker-compose.yml
    chmod a+rw ./gateway5/docker-compose.yml

    excluded_services=("app-iag_commander" "torero")   
    start_services "${excluded_services[@]}"
    ;;
  *)
    print_red "Invalid choice."
    ;;
esac
