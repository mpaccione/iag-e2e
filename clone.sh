#!/bin/bash
source ./utils.sh

logo_start "logo.sh"
check_jq

# File containing the JSON list of repositories to clone
REPO_FILE="./repos.json"

# Directory to clone the repositories into
TARGET_DIR="./"

# Create the target directory if it doesn't exist and cd
mkdir -p $TARGET_DIR
cd $TARGET_DIR

print_cyan "=== Cloning Repos ==="

create_manager_env() {
    if [ $1 == "gateway-management-ui" ]; then
        print_cyan "Creating gateway-management-ui .env file"

        env_content=$(cat << EOF
VITE_CUSTOMER_CONTROL_URL=https://control.tools-dev.itential.io/api/v1
VITE_KEYCLOAK_CLIENT_ID=hub
VITE_TOKEN_EXCHANGE_AUDIENCE_CUSTOMER_CONTROL=control
VITE_KEYCLOAK_URL=https://auth.tools-dev.itential.io
NODE_ENV=development
EOF
        )

        echo "$env_content" > "$(pwd)/.env"
        print_yellow "$env_content"
    fi
}

# Loop through each repository in the JSON file
jq -r 'to_entries[] | "\(.key) \(.value)"' "$REPO_FILE" | while read -r REPO_URL BRANCH; do
    echo ""
    # Extract the repo name from the URL (everything after the last '/')
    REPO_NAME=$(basename "$REPO_URL" .git)

    if [ -d "$REPO_NAME" ]; then
        # If the directory exists, enter it and update the repository
        cd "$REPO_NAME"
        print_green "Directory $REPO_NAME exists. Checking out branch $BRANCH and pulling latest changes."
        git reset --hard
        git checkout "$BRANCH"
        git pull origin "$BRANCH"
        create_manager_env "$REPO_NAME"
        cd ..
    else
        # If the directory does not exist, clone the repository and check out the specified branch
        if git clone -b "$BRANCH" "$REPO_URL"; then
            print_green "Successfully cloned $REPO_NAME and checked out branch $BRANCH"
        else
            print_red "Failed to clone $REPO_NAME"
        fi
    fi
done

echo ""
